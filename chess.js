console.log('Adding reference to the DOM');
var boardElement = document.getElementById('board');
var tiles = document.getElementsByClassName('tile');
var mod = 1;

for(var i = 0; i < tiles.length; i++) {
    if(i % 8 == 0) {
        mod = (mod ? 0 : 1);
        }
    if(i % 2 == mod) {
        }
    else {
        tiles[i].setAttribute('style', 'background-color: #60aa50;');
    }
    }

var b = new board();
var playerBlack = new player('tom');
var playerWhite = new player('mary');

var currentPlayer = playerWhite;
