class board {
    constructor()
    {
        this.squares = {};
        this.letters = 'abcdefgh';
        this.selectedPiece = undefined;
        for(var i = 0; i < 8; i++) {
            for(var j = 1; j < 9; j++) {
                var index = this.letters[i] + j;
                this.squares[index] = undefined;
            }
            }

        var p1 = new piece('b2', '&#9823', playerBlack);
        var p2 = new piece('b3', '&#9817', playerWhite);
        console.log(p1);
        // place peice
        this.placePiece(p1);
        this.placePiece(p2);
        console.log('What is board?', this);
        this.addClickListenerToTiles(this);
        this.updateBoard();
        console.log('Start your engine');
    }

    clickTile(event, board)
    {
        console.log('Event: ', event.target.id);
        var piece = board.squares[event.target.id];

        if(board.selectedPiece == undefined) {
            // console.log(this);
            if(typeof piece != 'undefined') {
                console.log(piece._name);
                // this.setSelectedPiece(piece);
                board.selectedPiece = piece;
                // console.log('selectedpiece:', this.selectedPiece);
                }
            else {
                console.log('nothing');
            }
            }
        else {
            console.log('this.selectedpiece: ', board.selectedPiece);
            board.squares[board.selectedPiece._position] = undefined;
            board.squares[event.target.id] = board.selectedPiece;
            board.selectedPiece._position = event.target.id;
            board.selectedPiece = undefined;
            board.updateBoard();
        }
    }

    placePiece(p)
    {
        this.squares[p._position] = p;
    }

    updateBoard()
    {
        for(var position in this.squares) {
            if(typeof this.squares[position] == 'undefined') {
                document.getElementById(position).innerHTML = '';
            }
            else {
                document.getElementById(position).innerHTML =
                    ('' + this.squares[position]._name)
                        .replace('undefined', '');
                console.log(
                    'innerHTML:', document.getElementById(position).innerHTML);
            }
        }
    }

    reportToConsole(message)
    {
        console.log(message);
    }

    addClickListenerToTiles(board)
    {
        // Iterate through tiles and add event listener
        var tiles = document.getElementsByClassName('tile');
        for(var i = 0; i < tiles.length; i++) {
            console.log('Add events');
            tiles[i].addEventListener('click', function(event) {
                board.clickTile(event, board);
            }, false);
        }
    };
};