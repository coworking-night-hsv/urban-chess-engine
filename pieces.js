class piece {
    constructor(position, type, player)
    {
        // type is only pawn
        this._name = type;
        this._position = position;
        this._player = player;
    }

    move(new_position)
    {
        this._position = new_position;
    }
};